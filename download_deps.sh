#!/bin/bash

echo "Downloading project dependencies"

hg init ${GOROOT}/src/pkg/code.google.com/p/goprotobuf/proto
printf "[paths]\ndefault = https://code.google.com/p/goprotobuf/" > $GOROOT/src/pkg/code.google.com/p/goprotobuf/proto/.hg/hgrc

# code.google.com
goapp get -u code.google.com/p/go.crypto/bcrypt

# github.com
goapp get -u github.com/julienschmidt/httprouter
goapp get -u github.com/mjibson/goon
goapp get -u github.com/golang/oauth2/google
