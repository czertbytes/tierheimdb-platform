package httputil

import (
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
	"strconv"
	"strings"

	"bitbucket.org/czertbytes/tierheimdb/pkg/types"

	"github.com/julienschmidt/httprouter"
)

const (
	AuthHeader string = "Authorization"
)

func GetSessionToken(r *http.Request) (string, error) {
	token := r.Header.Get(AuthHeader)
	if len(token) == 0 {
		return "", types.BadRequest("Authentication header not found!", types.ErrMissingAuthHeader.Error(), types.ErrMissingAuthHeader)
	}

	return token, nil
}

func GetFields(ps httprouter.Params) []string {
	return strings.Split(ps.ByName("fields"), ",")
}

// Returns entity id from request. The value type is int64 so it must be casted to specific entityId.
func GetEntityId(idStr string) (int64, error) {
	id, err := strconv.ParseInt(idStr, 10, 64)
	if err != nil {
		return 0, types.BadRequest("Parsing entity id failed!", types.ErrEntityIdNotValid.Error(), types.ErrEntityIdNotValid)
	}

	return id, nil
}

func GetAccountId(ps httprouter.Params) (types.AccountId, error) {
	id, err := GetEntityId(ps.ByName("accountId"))

	return types.AccountId(id), err
}

func GetShelterId(ps httprouter.Params) (types.ShelterId, error) {
	id, err := GetEntityId(ps.ByName("shelterId"))

	return types.ShelterId(id), err
}

func GetUpdateId(ps httprouter.Params) (types.UpdateId, error) {
	id, err := GetEntityId(ps.ByName("updateId"))

	return types.UpdateId(id), err
}

func GetAnimalId(ps httprouter.Params) (types.AnimalId, error) {
	id, err := GetEntityId(ps.ByName("animalId"))

	return types.AnimalId(id), err
}

func ParseBody(r *http.Request, v interface{}) error {
	contentType := r.Header.Get("Content-Type")
	contentType = strings.ToLower(contentType)

	switch contentType {
	case "application/json":
		fallthrough
	case "application/json;charset=utf-8":
		dec := json.NewDecoder(r.Body)
		for {
			if err := dec.Decode(v); err == io.EOF {
				break
			} else if err != nil {
				return err
			}
		}

		return nil

	default:
		msg := fmt.Sprintf("Content type %s is not supported.", contentType)
		return types.BadRequest(msg, "", fmt.Errorf(msg))
	}

	return nil
}

func ResponseAsJSON(w http.ResponseWriter, statusCode int, v interface{}) {
	w.Header().Set("Content-Type", "application/json;charset=utf-8")
	w.WriteHeader(statusCode)

	if err := json.NewEncoder(w).Encode(v); err != nil {
		log.Fatal(err)
	}
}

func ResponseOK(w http.ResponseWriter, v interface{}) {
	ResponseAsJSON(w, http.StatusOK, v)
}

func ResponseCreated(w http.ResponseWriter, url string, v interface{}) {
	w.Header().Set("Location", url)
	ResponseAsJSON(w, http.StatusCreated, v)
}

func ResponseNoContent(w http.ResponseWriter) {
	w.WriteHeader(http.StatusNoContent)
}

func ResponseUnauthorized(w http.ResponseWriter) {
	ResponseAsJSON(w, http.StatusUnauthorized, types.Unauthorized)
}

func ResponseError(w http.ResponseWriter, err error) {
	statusCode := http.StatusInternalServerError

	if _, ok := err.(*types.HttpError); ok {
		statusCode = err.(*types.HttpError).StatusCode()
	}

	ResponseAsJSON(w, statusCode, err)
}
