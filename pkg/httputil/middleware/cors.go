package middleware

import (
	"net/http"
)

type CORS struct {
	handler http.Handler
}

func NewCORS(h http.Handler) *CORS {
	return &CORS{
		handler: h,
	}
}

func (c *CORS) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	origin := "*"
	if ro := r.Header.Get("Origin"); ro != "" {
		origin = ro
	}
	w.Header().Add("Access-Control-Allow-Origin", origin)
	w.Header().Add("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, PATCH, DELETE")
	w.Header().Add("Access-Control-Allow-Headers", "Accept, Content-Type, Content-Length, Accept-Encoding, Authorization")

	if r.Method == "OPTIONS" {
		return
	}

	c.handler.ServeHTTP(w, r)
}
