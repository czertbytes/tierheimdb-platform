package sessions

import (
	"appengine/datastore"
	"time"

	"bitbucket.org/czertbytes/tierheimdb/pkg/types"

	"github.com/mjibson/goon"
)

const (
	Kind = "Session"
)

type Sessions struct {
	g *goon.Goon
}

func NewSessions(g *goon.Goon) *Sessions {
	return &Sessions{
		g: g,
	}
}

func (s *Sessions) Create(session *types.Session) error {
	now := time.Now()
	session.CreatedAt = now

	session.GenerateToken()

	_, err := s.g.Put(session)

	return err
}

func (s *Sessions) FindKey(session *types.Session) (*datastore.Key, error) {
	key := s.g.Key(session)
	if key == nil {
		return nil, datastore.ErrNoSuchEntity
	}

	return key, nil
}

func (s *Sessions) Find(id types.SessionId) (*types.Session, error) {
	session := &types.Session{
		Id: id,
	}

	err := s.g.Get(session)
	if err != nil {
		return nil, err
	}

	return session, err
}

func (s *Sessions) FindAllByToken(token string) (types.Sessions, error) {
	var sessions types.Sessions
	q := datastore.NewQuery(Kind).
		Filter("Token =", token).
		Limit(1)

	if _, err := s.g.GetAll(q, &sessions); err != nil {
		return nil, err
	}

	if len(sessions) == 0 {
		sessions = make(types.Sessions, 0)
	}

	return sessions, nil
}

func (s *Sessions) Update(session *types.Session) error {
	_, err := s.g.Put(session)

	return err
}

func (s *Sessions) Delete(id types.SessionId) error {
	var session types.Session
	session.Id = id

	key, err := s.FindKey(&session)
	if err != nil {
		return err
	}

	return s.g.Delete(key)
}
