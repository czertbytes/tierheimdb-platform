package updates

import (
	"appengine/datastore"
	"time"

	"bitbucket.org/czertbytes/tierheimdb/pkg/types"

	"github.com/mjibson/goon"
)

const (
	Kind = "Update"
)

type Updates struct {
	g *goon.Goon
}

func NewUpdates(g *goon.Goon) *Updates {
	return &Updates{
		g: g,
	}
}

func (u *Updates) Create(update *types.Update) error {
	now := time.Now()
	update.Status = types.UpdateStatusActive
	update.CreatedAt = now
	update.UpdatedAt = now
	update.SetFormattedValues()

	key, err := u.g.Put(update)
	update.Key = key

	return err
}

func (u *Updates) FindKey(update *types.Update) (*datastore.Key, error) {
	key := u.g.Key(update)
	if key == nil {
		return nil, datastore.ErrNoSuchEntity
	}

	return key, nil
}

func (u *Updates) Find(id types.UpdateId, parent *datastore.Key) (*types.Update, error) {
	update := &types.Update{
		Id:     id,
		Parent: parent,
	}

	err := u.g.Get(update)
	if err != nil {
		return nil, err
	}

	key, err := u.FindKey(update)
	if err != nil {
		return nil, err
	}
	update.Key = key
	update.SetFormattedValues()

	return update, err
}

func (u *Updates) FindLatest(parent *datastore.Key) (*types.Update, error) {
	var updates types.Updates
	q := datastore.NewQuery(Kind).
		Ancestor(parent).
		Filter("Status >", types.UpdateStatusDeleted).
		Order("Status").
		Order("-CreatedAt").
		Limit(1)

	keys, err := u.g.GetAll(q, &updates)
	if err != nil {
		return nil, err
	}

	for i, key := range keys {
		updates[i].Key = key
		updates[i].SetFormattedValues()
	}

	if len(updates) == 0 {
		return nil, datastore.ErrNoSuchEntity
	}

	return &updates[0], nil
}

func (u *Updates) FindAllByParent(parent *datastore.Key) (types.Updates, error) {
	var updates types.Updates
	q := datastore.NewQuery(Kind).
		Ancestor(parent).
		Filter("Status >", types.UpdateStatusDeleted).
		Order("Status").
		Order("CreatedAt")

	keys, err := u.g.GetAll(q, &updates)
	if err != nil {
		return nil, err
	}

	for i, key := range keys {
		updates[i].Key = key
		updates[i].SetFormattedValues()
	}

	if len(updates) == 0 {
		updates = make(types.Updates, 0)
	}

	return updates, nil
}

func (u *Updates) FindMulti(ids types.UpdateIds) (types.Updates, error) {
	updates := make(types.Updates, len(ids))
	for i, id := range ids {
		updates[i] = types.Update{
			Id: id,
		}
	}

	if err := u.g.GetMulti(&updates); err != nil {
		return nil, err
	}

	for i := 0; i < len(ids); i++ {
		updates[i].SetFormattedValues()
	}

	if len(updates) == 0 {
		updates = make(types.Updates, 0)
	}

	return updates, nil
}

func (u *Updates) Update(update *types.Update) error {
	now := time.Now()
	update.UpdatedAt = now

	key, err := u.g.Put(update)
	update.Key = key

	return err
}

func (u *Updates) Delete(id types.UpdateId, parent *datastore.Key) error {
	var update types.Update
	update.Id = id
	update.Parent = parent

	key, err := u.FindKey(&update)
	if err != nil {
		return err
	}

	return u.g.Delete(key)
}
