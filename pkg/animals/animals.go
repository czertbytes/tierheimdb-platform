package animals

import (
	"appengine/datastore"
	"time"

	"bitbucket.org/czertbytes/tierheimdb/pkg/types"

	"github.com/mjibson/goon"
)

const (
	Kind = "Animal"
)

type Animals struct {
	g *goon.Goon
}

func NewAnimals(g *goon.Goon) *Animals {
	return &Animals{
		g: g,
	}
}

func (u *Animals) Create(animal *types.Animal) error {
	now := time.Now()
	animal.Status = types.AnimalStatusActive
	animal.CreatedAt = now
	animal.UpdatedAt = now
	animal.SetFormattedValues()

	key, err := u.g.Put(animal)
	animal.Key = key

	return err
}

func (u *Animals) FindKey(animal *types.Animal) (*datastore.Key, error) {
	key := u.g.Key(animal)
	if key == nil {
		return nil, datastore.ErrNoSuchEntity
	}

	return key, nil
}

func (u *Animals) Find(id types.AnimalId, parent *datastore.Key) (*types.Animal, error) {
	animal := &types.Animal{
		Id:     id,
		Parent: parent,
	}

	err := u.g.Get(animal)
	if err != nil {
		return nil, err
	}

	key, err := u.FindKey(animal)
	if err != nil {
		return nil, err
	}
	animal.Key = key
	animal.SetFormattedValues()

	return animal, err
}

func (u *Animals) FindAllByParent(parent *datastore.Key) (types.Animals, error) {
	var animals types.Animals
	q := datastore.NewQuery(Kind).
		Ancestor(parent).
		Filter("Status >", types.AnimalStatusDeleted).
		Order("Status").
		Order("CreatedAt")

	keys, err := u.g.GetAll(q, &animals)
	if err != nil {
		return nil, err
	}

	for i, key := range keys {
		animals[i].Key = key
		animals[i].SetFormattedValues()
	}

	if len(animals) == 0 {
		animals = make(types.Animals, 0)
	}

	return animals, nil
}

func (u *Animals) FindMulti(ids types.AnimalIds) (types.Animals, error) {
	animals := make(types.Animals, len(ids))
	for i, id := range ids {
		animals[i] = types.Animal{
			Id: id,
		}
	}

	if err := u.g.GetMulti(&animals); err != nil {
		return nil, err
	}

	for i := 0; i < len(ids); i++ {
		animals[i].SetFormattedValues()
	}

	if len(animals) == 0 {
		animals = make(types.Animals, 0)
	}

	return animals, nil
}

func (u *Animals) Update(animal *types.Animal) error {
	now := time.Now()
	animal.UpdatedAt = now

	key, err := u.g.Put(animal)
	animal.Key = key

	return err
}

func (u *Animals) Delete(id types.AnimalId, parent *datastore.Key) error {
	var animal types.Animal
	animal.Id = id
	animal.Parent = parent

	key, err := u.FindKey(&animal)
	if err != nil {
		return err
	}

	return u.g.Delete(key)
}
