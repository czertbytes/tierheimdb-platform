package accounts

import (
	"appengine/datastore"
	"time"

	"bitbucket.org/czertbytes/tierheimdb/pkg/types"

	"github.com/mjibson/goon"
)

const (
	Kind = "Account"
)

type Accounts struct {
	g *goon.Goon
}

func NewAccounts(g *goon.Goon) *Accounts {
	return &Accounts{
		g: g,
	}
}

func (u *Accounts) Create(account *types.Account) error {
	now := time.Now()
	account.Status = types.AccountStatusActive
	account.CreatedAt = now
	account.UpdatedAt = now
	account.SetFormattedValues()

	key, err := u.g.Put(account)
	account.Key = key

	return err
}

func (u *Accounts) FindKey(account *types.Account) (*datastore.Key, error) {
	key := u.g.Key(account)
	if key == nil {
		return nil, datastore.ErrNoSuchEntity
	}

	return key, nil
}

func (u *Accounts) Find(id types.AccountId) (*types.Account, error) {
	account := &types.Account{
		Id: id,
	}

	err := u.g.Get(account)
	if err != nil {
		return nil, err
	}

	key, err := u.FindKey(account)
	if err != nil {
		return nil, err
	}
	account.Key = key
	account.SetFormattedValues()

	return account, err
}

func (a *Accounts) FindAllByEmail(email string) (types.Accounts, error) {
	var accounts types.Accounts
	q := datastore.NewQuery(Kind).
		Filter("Email =", email).
		Filter("Status >", types.AccountStatusDeleted)

	keys, err := a.g.GetAll(q, &accounts)
	if err != nil {
		a.g.Context.Debugf("accounts: Getting accounts by email failed with error '%s'.", err.Error())
		return nil, err
	}

	for i, key := range keys {
		accounts[i].Key = key
		accounts[i].SetFormattedValues()
	}

	if len(accounts) == 0 {
		a.g.Context.Debugf("accounts: Getting accounts by email no results.")
		accounts = make(types.Accounts, 0)
	}

	return accounts, nil
}

func (u *Accounts) FindMulti(ids types.AccountIds) (types.Accounts, error) {
	accounts := make(types.Accounts, len(ids))
	for i, id := range ids {
		accounts[i] = types.Account{
			Id: id,
		}
	}

	if err := u.g.GetMulti(&accounts); err != nil {
		return nil, err
	}

	for i := 0; i < len(ids); i++ {
		accounts[i].SetFormattedValues()
	}

	if len(accounts) == 0 {
		accounts = make(types.Accounts, 0)
	}

	return accounts, nil
}

func (u *Accounts) Update(account *types.Account) error {
	now := time.Now()
	account.UpdatedAt = now

	key, err := u.g.Put(account)
	account.Key = key

	return err
}

func (u *Accounts) Delete(id types.AccountId) error {
	var account types.Account
	account.Id = id

	key, err := u.FindKey(&account)
	if err != nil {
		return err
	}

	return u.g.Delete(key)
}
