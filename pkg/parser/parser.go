package parser

import (
	"strings"
	"unicode/utf8"

	"bitbucket.org/czertbytes/tierheimdb/pkg/types"
)

func MergeAnimal(a, o *types.Animal) *types.Animal {
	if o.Id > 0 {
		a.Id = o.Id
	}

	if len(o.OriginURL) > 0 {
		a.OriginURL = o.OriginURL
	}

	if len(o.Name) > 0 {
		a.Name = o.Name
	}

	if o.Type > 0 {
		a.Type = o.Type
	}

	if len(o.Breed) > 0 {
		a.Breed = o.Breed
	}

	if o.Sex > 0 {
		a.Sex = o.Sex
	}

	if len(o.ShortDesc) > 0 {
		a.ShortDesc = o.ShortDesc
	}

	if len(o.LongDesc) > 0 {
		a.LongDesc = o.LongDesc
	}

	if o.Images != nil && len(o.Images) > 0 {
		a.Images = o.Images
	}

	return a
}

func ToUTF8(s string) string {
	if !utf8.ValidString(s) {
		bytes := []byte(s)
		buf := make([]rune, len(bytes))
		for i, b := range bytes {
			buf[i] = rune(b)
		}
		return string(buf)
	}

	return s
}

var (
	MaleSexKeywords = []string{
		"männlich",
		"männl",
		"rüde",
		"rüden",
		"freigänger",
		"wohnungskater",
	}

	FemaleSexKeywords = []string{
		"weiblich",
		"weibl",
		"hündin",
		"freigängerin",
		"wohnungskatze",
	}

	SexKeywords = []string{}
)

func init() {
	SexKeywords = append(SexKeywords, MaleSexKeywords...)
	SexKeywords = append(SexKeywords, FemaleSexKeywords...)
}

func NormalizeName(s string) string {
	if len(s) == 0 {
		return s
	}

	t := PrepareStringChunk(s)
	if i := strings.IndexAny(t, "-/"); i > 0 {
		t = t[:i]
	}
	t = strings.Trim(t, " ")

	return t
}

func NormalizeBreed(s string) string {
	if len(s) == 0 {
		return s
	}

	t := PrepareStringChunk(s)
	for _, s := range []string{"Katze", "Hund", "Rasse: "} {
		t = strings.Replace(t, s, "", -1)
	}
	t = strings.Replace(t, " -Mix", "-Mix", -1)
	t = strings.Replace(t, "EKH", "Europäisch Kurzhaar", -1)
	t = strings.Trim(t, " /")

	if len(t) > 1 {
		t = strings.ToUpper(t[0:1]) + t[1:]
	}

	return t
}

func NormalizeSex(s string) string {
	if len(s) == 0 {
		return s
	}

	t := PrepareStringChunk(s)
	t = strings.ToLower(t)
	for _, s := range []string{"/", ",", ":", "."} {
		t = strings.Replace(t, s, " ", -1)
	}

	parsedSex := []string{}
	for _, token := range strings.Split(t, " ") {
		tokenLower := strings.ToLower(token)
		for _, s := range MaleSexKeywords {
			if tokenLower == s {
				parsedSex = append(parsedSex, "M")
			}
		}

		for _, s := range FemaleSexKeywords {
			if tokenLower == s {
				parsedSex = append(parsedSex, "F")
			}
		}
	}

	return strings.Join(parsedSex, "/")
}

func PrepareStringChunk(s string) string {
	if len(s) == 0 {
		return s
	}

	t := strings.Trim(ToUTF8(s), " ")
	t = strings.Replace(t, "\u00A0", " ", -1)
	for _, s := range []string{"\u0009", "\u000A", "\u0084", "\u0093"} {
		t = strings.Replace(t, s, "", -1)
	}
	t = strings.Replace(t, "  ", " ", -1)
	t = strings.Trim(t, " ")

	return t
}
