package types

type Params struct {
	SessionToken string
	AccountId    AccountId
	ShelterId    ShelterId
	UpdateId     UpdateId
	AnimalId     AnimalId
	Fields       []string
	Offset       int
	Limit        int
}
