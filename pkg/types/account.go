package types

import (
	"appengine/datastore"
	"time"
)

type AccountId int64
type AccountIds []AccountId
type AccountStatus int

var (
	AccountStatusDeleted AccountStatus = -1
	AccountStatusUnknown AccountStatus = 0
	AccountStatusActive  AccountStatus = 1
	AccountStatusAdmin   AccountStatus = 99
)

func ParseAccountStatus(value int) AccountStatus {
	switch value {
	case -1:
		return AccountStatusDeleted
	case 1:
		return AccountStatusActive
	case 99:
		return AccountStatusAdmin
	default:
		return AccountStatusUnknown
	}
}
func (as AccountStatus) String() string {
	switch as {
	case AccountStatusDeleted:
		return "deleted"
	case AccountStatusActive:
		return "active"
	case AccountStatusAdmin:
		return "admin"
	default:
		return "unknown"
	}
}

type Account struct {
	Id              AccountId      `json:"id,string" datastore:"-" goon:"id"`
	Status          AccountStatus  `json:"status"`
	StatusFormatted string         `json:"status_formatted"`
	CreatedAt       time.Time      `json:"created_at"`
	UpdatedAt       time.Time      `json:"updated_at"`
	Key             *datastore.Key `json:"-" datastore:"-"`

	// Entity fields
	Name     string `json:"name"`
	Email    string `json:"email"`
	Password string `json:"password,omitempty"`
	Session  string `json:"session,omitempty" datastore:"-"`

	// The parent is Shelter
	Parent *datastore.Key `json:"-" goon:"parent"`
}

func (a *Account) SetFormattedValues() {
	a.SetStatusFormatted()
}

func (a *Account) SetStatusFormatted() {
	a.StatusFormatted = a.Status.String()
}

func (a *Account) IsAdmin() bool {
	return a.Status == AccountStatusAdmin
}

type Accounts []Account
