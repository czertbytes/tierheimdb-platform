package types

import (
	"net/http"
)

type HttpError struct {
	Message     string `json:"message"`
	Description string `json:"description"`
	statusCode  int    `json:"-"`
	err         error  `json:"-"`
}

func (e HttpError) Error() string {
	return e.err.Error()
}

func (e *HttpError) StatusCode() int {
	return e.statusCode
}

func BadRequest(message, description string, err error) error {
	return &HttpError{
		Message:     message,
		Description: description,
		statusCode:  http.StatusBadRequest,
		err:         err,
	}
}

func NotFound(message, description string, err error) error {
	return &HttpError{
		Message:     message,
		Description: description,
		statusCode:  http.StatusNotFound,
		err:         err,
	}
}

func Unauthorized(message, description string, err error) error {
	return &HttpError{
		Message:     message,
		Description: description,
		statusCode:  http.StatusUnauthorized,
		err:         err,
	}
}

func InternalServerError(message, description string, err error) error {
	return &HttpError{
		Message:     message,
		Description: description,
		statusCode:  http.StatusInternalServerError,
		err:         err,
	}
}
