package types

import (
	"appengine/datastore"
	"time"
)

type ShelterId int64
type ShelterIds []ShelterId
type ShelterStatus int

var (
	ShelterStatusDeleted ShelterStatus = -1
	ShelterStatusUnknown ShelterStatus = 0
	ShelterStatusActive  ShelterStatus = 1
)

func ParseShelterStatus(value int) ShelterStatus {
	switch value {
	case -1:
		return ShelterStatusDeleted
	case 1:
		return ShelterStatusActive
	default:
		return ShelterStatusUnknown
	}
}
func (as ShelterStatus) String() string {
	switch as {
	case ShelterStatusDeleted:
		return "deleted"
	case ShelterStatusActive:
		return "active"
	default:
		return "unknown"
	}
}

type Shelter struct {
	Id              ShelterId      `json:"id,string" datastore:"-" goon:"id"`
	Status          ShelterStatus  `json:"status"`
	StatusFormatted string         `json:"status_formatted"`
	CreatedAt       time.Time      `json:"created_at"`
	UpdatedAt       time.Time      `json:"updated_at"`
	Key             *datastore.Key `json:"-" datastore:"-"`

	// Entity fields
	Name        string      `json:"name"`
	FullName    string      `json:"full_name"`
	URL         string      `json:"url"`
	LogoURL     string      `json:"logo_url"`
	Phone       string      `json:"phone"`
	Email       string      `json:"email"`
	ShortDesc   string      `json:"short_desc"`
	LongDesc    string      `json:"long_desc"`
	Address     Address     `json:"address"`
	Note        string      `json:"note"`
	AnimalTypes AnimalTypes `json:"animal_types"`

	// The parent is Account
	Parent *datastore.Key `json:"-" goon:"parent"`
}

func (s *Shelter) SetFormattedValues() {
	s.SetStatusFormatted()
}

func (s *Shelter) SetStatusFormatted() {
	s.StatusFormatted = s.Status.String()
}

type Shelters []Shelter
