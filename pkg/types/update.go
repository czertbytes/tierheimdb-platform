package types

import (
	"appengine/datastore"
	"time"
)

type UpdateId int64
type UpdateIds []UpdateId
type UpdateStatus int

var (
	UpdateStatusDeleted UpdateStatus = -1
	UpdateStatusUnknown UpdateStatus = 0
	UpdateStatusActive  UpdateStatus = 1
)

func ParseUpdateStatus(value int) UpdateStatus {
	switch value {
	case -1:
		return UpdateStatusDeleted
	case 1:
		return UpdateStatusActive
	default:
		return UpdateStatusUnknown
	}
}
func (as UpdateStatus) String() string {
	switch as {
	case UpdateStatusDeleted:
		return "deleted"
	case UpdateStatusActive:
		return "active"
	default:
		return "unknown"
	}
}

type Update struct {
	Id              UpdateId       `json:"id,string" datastore:"-" goon:"id"`
	Status          UpdateStatus   `json:"status"`
	StatusFormatted string         `json:"status_formatted"`
	CreatedAt       time.Time      `json:"created_at"`
	UpdatedAt       time.Time      `json:"updated_at"`
	Key             *datastore.Key `json:"-" datastore:"-"`

	// Entity fields
	Name string `json:"name"`

	// The parent is Shelter
	Parent *datastore.Key `json:"-" goon:"parent"`
}

func (u *Update) SetFormattedValues() {
	u.SetStatusFormatted()
}

func (u *Update) SetStatusFormatted() {
	u.StatusFormatted = u.Status.String()
}

type Updates []Update
