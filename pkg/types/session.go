package types

import (
	"time"
)

const (
	TokenLength = 24
)

var (
	TokenExpire = time.Hour * 24
)

type SessionId int64

type Session struct {
	Id        SessionId `json:"id,string" datastore:"-" goon:"id"`
	AccountId AccountId `json:"account_id"`
	CreatedAt time.Time `json:"created_at"`
	ExpiresAt time.Time `json:"expires_at"`

	// Entity fields
	Token string `json:"token"`
}

func (s *Session) GenerateToken() {
	s.Token = RandomId(TokenLength)
	s.ExpiresAt = time.Now().Add(TokenExpire)
}

type Sessions []*Session
