package types

import (
	"bytes"
	"crypto/hmac"
	"crypto/sha256"
	"encoding/base64"
	"fmt"
	"io"
	"math/rand"
	"strconv"
	"strings"
	"time"

	"code.google.com/p/go.crypto/bcrypt"
)

const (
	// Entropy: 63^{length} * log(2)
	alphanum = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"

	// Remove ambiguous characters: 8-B, U-V, u-v, 1-i-l, 0-O   Entropy: 51^{length} * log(2)
	alphanumForHumans = "2345679abcdefghjkmnopqrstwxyzACDEFGHJKLMNPQRSTWXYZ"
)

var (
	pepper = []byte("YAJci5ugGbjUw2qf2tz0ikBjou_V-pZ_bg_JxA7LvnnwYqtaFtie7ByW7zuE1PLkBBwxJcHwECR0joLgeenWQlTIdKl5Pv7y1tNn9zjzQxyRLCAXWNOvGKnM_mTmkIMA")
)

func Hash(values ...interface{}) string {
	hash := sha256.New224()

	for _, value := range values {
		io.WriteString(hash, fmt.Sprintf("%s", value))
	}

	io.WriteString(hash, fmt.Sprintf("timestamp%d", time.Now().UnixNano()))

	return fmt.Sprintf("%x", hash.Sum(nil))
}

func BCrypt(value string) string {
	h := hmac.New(sha256.New, pepper)
	h.Write([]byte(value))

	s := base64.StdEncoding.EncodeToString(h.Sum(nil))

	b, _ := bcrypt.GenerateFromPassword([]byte(s), 8)

	return string(b)
}

func BCryptCompare(value, hashed string) bool {
	h := hmac.New(sha256.New, pepper)
	h.Write([]byte(value))

	s := base64.StdEncoding.EncodeToString(h.Sum(nil))

	err := bcrypt.CompareHashAndPassword([]byte(hashed), []byte(s))
	return err == nil
}

func RandomId(l int) string {
	rand.Seed(time.Now().UnixNano())
	n := make([]byte, l)
	for i := range n {
		n[i] = alphanum[rand.Intn(len(alphanum))]
	}
	return string(n)
}

func RandomIdForHumans(l int) string {
	rand.Seed(time.Now().UnixNano())
	n := make([]byte, l)
	for i := range n {
		n[i] = alphanumForHumans[rand.Intn(len(alphanumForHumans))]
	}
	return string(n)
}

func StringIdsMarshalJSON(ids []int64) []byte {
	var b bytes.Buffer
	b.WriteString("[")

	l := len(ids)
	if l > 0 {
		b.WriteString(fmt.Sprintf(`"%d"`, ids[0]))
		if l > 1 {
			for i := 1; i < l; i++ {
				b.WriteString(fmt.Sprintf(`,"%d"`, ids[i]))
			}
		}
	}

	b.WriteString("]")

	return b.Bytes()
}

func StringIdsUnmarshalJSON(bytes []byte) ([]int64, error) {
	s := strings.Trim(string(bytes), "[]")
	s = strings.Replace(s, " ", "", -1) // remove blanks between groups
	intAsStrings := strings.Split(s, ",")

	ids := make([]int64, 0)
	for _, intAsString := range intAsStrings {
		if len(intAsString) > 0 {
			x := strings.Trim(intAsString, `"`)

			value, err := strconv.ParseInt(x, 10, 64)
			if err != nil {
				return nil, err
			}

			ids = append(ids, value)
		}
	}

	return ids, nil
}
