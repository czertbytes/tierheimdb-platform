package types

type Image struct {
	Width   int    `json:"width"`
	Height  int    `json:"height"`
	URL     string `json:"url"`
	Comment string `json:"comment"`
}

type Images []Image
