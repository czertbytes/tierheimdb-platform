package types

import (
	"errors"
)

var (
	// Generic errors
	ErrTimeout           = errors.New("Requested operation took too long!")
	ErrUnauthorized      = errors.New("Your account is not authorized for this operation!")
	ErrMissingAuthHeader = errors.New("Missing authorization token in request headers!")
	ErrEntityIdNotValid  = errors.New("Requested entity id is not valid number!")

	// Account
	ErrAccountNotFound = errors.New("account: No result found.")
)
