package types

import (
	"appengine/datastore"
	"time"
)

type AnimalId int64
type AnimalIds []AnimalId
type AnimalStatus int
type AnimalPriority int
type AnimalType int
type AnimalTypes []AnimalType
type AnimalSex int

var (
	AnimalStatusDeleted AnimalStatus = -1
	AnimalStatusUnknown AnimalStatus = 0
	AnimalStatusActive  AnimalStatus = 1
)

func ParseAnimalStatus(value int) AnimalStatus {
	switch value {
	case -1:
		return AnimalStatusDeleted
	case 1:
		return AnimalStatusActive
	default:
		return AnimalStatusUnknown
	}
}
func (as AnimalStatus) String() string {
	switch as {
	case AnimalStatusDeleted:
		return "deleted"
	case AnimalStatusActive:
		return "active"
	default:
		return "unknown"
	}
}

var (
	AnimalTypeUnknown AnimalType = 0
	AnimalTypeCat     AnimalType = 1
	AnimalTypeDog     AnimalType = 2
)

func ParseAnimalType(value int) AnimalType {
	switch value {
	case 1:
		return AnimalTypeCat
	case 2:
		return AnimalTypeDog
	default:
		return AnimalTypeUnknown
	}
}

func ParseAnimalTypeString(value string) AnimalType {
	switch value {
	case "cat":
		return AnimalTypeCat
	case "dog":
		return AnimalTypeDog
	default:
		return AnimalTypeUnknown
	}
}

func (as AnimalType) String() string {
	switch as {
	case AnimalTypeCat:
		return "cat"
	case AnimalTypeDog:
		return "dog"
	default:
		return "unknown"
	}
}

type Animal struct {
	Id              AnimalId       `json:"id,string" datastore:"-" goon:"id"`
	Status          AnimalStatus   `json:"status"`
	StatusFormatted string         `json:"status_formatted"`
	CreatedAt       time.Time      `json:"created_at"`
	UpdatedAt       time.Time      `json:"updated_at"`
	Key             *datastore.Key `json:"-" datastore:"-"`

	// Entity fields
	Name      string         `json:"name"`
	OriginURL string         `json:"origin_url"`
	Priority  AnimalPriority `json:"priority,omitempty"`
	Type      AnimalType     `json:"type,omitempty"`
	Breed     string         `json:"breed,omitempty"`
	Sex       AnimalSex      `json:"sex,omitempty"`
	ShortDesc string         `json:"short_desc,omitempty" datastore:",noindex"`
	LongDesc  string         `json:"long_desc,omitempty" datastore:",noindex"`
	Images    Images         `json:"images"`

	// The parent is Update
	Parent    *datastore.Key `json:"-" goon:"parent"`
	ShelterId ShelterId      `json:"shelter_id,omitempty"`
}

func (a *Animal) SetFormattedValues() {
	a.SetStatusFormatted()
}

func (a *Animal) SetStatusFormatted() {
	a.StatusFormatted = a.Status.String()
}

type Animals []Animal
