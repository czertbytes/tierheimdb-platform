package types

import (
	"appengine"
)

type Address struct {
	Street       string             `json:"street"`
	StreetNumber string             `json:"street_number"`
	PostalCode   string             `json:"postal_code"`
	City         string             `json:"city"`
	State        string             `json:"state"`
	LatLon       appengine.GeoPoint `json:"lat_lon"`
}
