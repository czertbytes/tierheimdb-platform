package shelters

import (
	"appengine/datastore"
	"time"

	"bitbucket.org/czertbytes/tierheimdb/pkg/types"

	"github.com/mjibson/goon"
)

const (
	Kind = "Shelter"
)

type Shelters struct {
	g *goon.Goon
}

func NewShelters(g *goon.Goon) *Shelters {
	return &Shelters{
		g: g,
	}
}

func (s *Shelters) Create(shelter *types.Shelter) error {
	now := time.Now()
	shelter.Status = types.ShelterStatusActive
	shelter.CreatedAt = now
	shelter.UpdatedAt = now
	shelter.SetFormattedValues()

	key, err := s.g.Put(shelter)
	shelter.Key = key

	return err
}

func (s *Shelters) FindKey(shelter *types.Shelter) (*datastore.Key, error) {
	key := s.g.Key(shelter)
	if key == nil {
		return nil, datastore.ErrNoSuchEntity
	}

	return key, nil
}

func (s *Shelters) Find(id types.ShelterId) (*types.Shelter, error) {
	shelter := &types.Shelter{
		Id: id,
	}

	err := s.g.Get(shelter)
	if err != nil {
		return nil, err
	}

	key, err := s.FindKey(shelter)
	if err != nil {
		return nil, err
	}
	shelter.Key = key
	shelter.SetFormattedValues()

	return shelter, err
}

func (s *Shelters) FindAll() (types.Shelters, error) {
	var shelters types.Shelters
	q := datastore.NewQuery(Kind).
		Filter("Status >", types.ShelterStatusDeleted).
		Order("Status").
		Order("CreatedAt")

	keys, err := s.g.GetAll(q, &shelters)
	if err != nil {
		return nil, err
	}

	for i, key := range keys {
		shelters[i].Key = key
		shelters[i].SetFormattedValues()
	}

	if len(shelters) == 0 {
		shelters = make(types.Shelters, 0)
	}

	return shelters, nil
}

func (s *Shelters) FindMulti(ids types.ShelterIds) (types.Shelters, error) {
	shelters := make(types.Shelters, len(ids))
	for i, id := range ids {
		shelters[i] = types.Shelter{
			Id: id,
		}
	}

	if err := s.g.GetMulti(&shelters); err != nil {
		return nil, err
	}

	for i := 0; i < len(ids); i++ {
		shelters[i].SetFormattedValues()
	}

	if len(shelters) == 0 {
		shelters = make(types.Shelters, 0)
	}

	return shelters, nil
}

func (s *Shelters) Update(shelter *types.Shelter) error {
	now := time.Now()
	shelter.UpdatedAt = now

	key, err := s.g.Put(shelter)
	shelter.Key = key

	return err
}

func (s *Shelters) Delete(id types.ShelterId) error {
	var shelter types.Shelter
	shelter.Id = id

	key, err := s.FindKey(&shelter)
	if err != nil {
		return err
	}

	return s.g.Delete(key)
}
