package auth

import (
	"bitbucket.org/czertbytes/tierheimdb/pkg/accounts"
	"bitbucket.org/czertbytes/tierheimdb/pkg/sessions"
	"bitbucket.org/czertbytes/tierheimdb/pkg/types"

	"github.com/mjibson/goon"
)

type Auth struct {
	g *goon.Goon
	a *accounts.Accounts
	s *sessions.Sessions
}

func NewAuth(g *goon.Goon) *Auth {
	return &Auth{
		g: g,
		a: accounts.NewAccounts(g),
		s: sessions.NewSessions(g),
	}
}

func (a *Auth) FindAccount(token string) (*types.Account, error) {
	sessionTokens, err := a.s.FindAllByToken(token)
	if err != nil {
		return nil, err
	}

	if len(sessionTokens) != 1 {
		a.g.Context.Debugf("There is not only one session token.")
		return nil, types.BadRequest("Account not found!", types.ErrAccountNotFound.Error(), types.ErrAccountNotFound)
	}

	account, err := a.a.Find(sessionTokens[0].AccountId)
	if err != nil {
		a.g.Context.Debugf("Getting account failed with error: %s", err)
		return nil, err
	}

	return account, nil
}
