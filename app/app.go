package app

import (
	"net/http"

	"bitbucket.org/czertbytes/tierheimdb/pkg/httputil/middleware"

	"github.com/julienschmidt/httprouter"
)

func init() {
	router := httprouter.New()

	// handles web endpoints
	WebRoutes(router)

	// handles api endpoints
	ManagerRoutes(router)

	http.Handle("/", middleware.NewLogging(middleware.NewCORS(router)))
}
