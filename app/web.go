package app

import (
	"bitbucket.org/czertbytes/tierheimdb/module/web"

	"github.com/julienschmidt/httprouter"
)

func WebRoutes(router *httprouter.Router) {
	router.GET("/", web.Get)
}
