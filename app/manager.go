package app

import (
	"bitbucket.org/czertbytes/tierheimdb/module/manager/accounts"
	"bitbucket.org/czertbytes/tierheimdb/module/manager/animals"
	"bitbucket.org/czertbytes/tierheimdb/module/manager/auth"
	"bitbucket.org/czertbytes/tierheimdb/module/manager/shelters"
	"bitbucket.org/czertbytes/tierheimdb/module/manager/updates"

	"github.com/julienschmidt/httprouter"
)

func ManagerRoutes(router *httprouter.Router) {
	// Accounts
	router.GET("/manager/v1/accounts/:accountId", accounts.Get)                // requires auth token
	router.PUT("/manager/v1/accounts/:accountId", accounts.Put)                // requires auth token
	router.PATCH("/manager/v1/accounts/:accountId", accounts.Patch)            // requires auth token
	router.DELETE("/manager/v1/accounts/:accountId", accounts.Delete)          // requires auth token
	router.GET("/manager/v1/accounts/:accountId/shelter", accounts.GetShelter) // requires auth token

	// Animals
	router.POST("/manager/v1/animals/:shelterId/:updateId", animals.Post) // requires auth token
	router.GET("/manager/v1/animals/:shelterId/:updateId/:animalId", animals.Get)
	router.PUT("/manager/v1/animals/:shelterId/:updateId/:animalId", animals.Put)       // requires auth token
	router.PATCH("/manager/v1/animals/:shelterId/:updateId/:animalId", animals.Patch)   // requires auth token
	router.DELETE("/manager/v1/animals/:shelterId/:updateId/:animalId", animals.Delete) // requires auth token

	// Auth
	router.POST("/manager/v1/auth/login", auth.Login)
	router.GET("/manager/v1/auth/logout", auth.Logout) // requires auth token
	router.POST("/manager/v1/auth/register", auth.Register)

	// Shelters
	router.POST("/manager/v1/shelters", shelters.Post) // requires auth token
	router.GET("/manager/v1/shelters", shelters.GetAll)
	router.GET("/manager/v1/shelters/:shelterId", shelters.Get)
	router.PUT("/manager/v1/shelters/:shelterId", shelters.Put)       // requires auth token
	router.PATCH("/manager/v1/shelters/:shelterId", shelters.Patch)   // requires auth token
	router.DELETE("/manager/v1/shelters/:shelterId", shelters.Delete) // requires auth token
	router.GET("/manager/v1/shelters/:shelterId/animals", shelters.GetAnimals)
	router.GET("/manager/v1/shelters/:shelterId/updates", shelters.GetUpdates)

	// Updates
	router.POST("/manager/v1/updates/:shelterId", updates.Post) // requires auth token
	router.GET("/manager/v1/updates/:shelterId/:updateId", updates.Get)
	router.DELETE("/manager/v1/updates/:shelterId/:updateId", updates.Delete) // requires auth token
	router.GET("/manager/v1/updates/:shelterId/:updateId/animals", updates.GetAnimals)
}
