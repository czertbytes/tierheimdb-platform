package animals

import (
	"bitbucket.org/czertbytes/tierheimdb/pkg/animals"
	"bitbucket.org/czertbytes/tierheimdb/pkg/shelters"
	"bitbucket.org/czertbytes/tierheimdb/pkg/types"
	"bitbucket.org/czertbytes/tierheimdb/pkg/updates"

	"github.com/mjibson/goon"
)

func CreateAnimal(g *goon.Goon, animal *types.Animal, params *types.Params) error {
	shelter, err := shelters.NewShelters(g).Find(params.ShelterId)
	if err != nil {
		return err
	}

	update, err := updates.NewUpdates(g).Find(params.UpdateId, shelter.Key)
	if err != nil {
		return err
	}

	animal.Parent = update.Key

	return animals.NewAnimals(g).Create(animal)
}

func FindAnimal(g *goon.Goon, params *types.Params) (*types.Animal, error) {
	shelter, err := shelters.NewShelters(g).Find(params.ShelterId)
	if err != nil {
		return nil, err
	}

	update, err := updates.NewUpdates(g).Find(params.UpdateId, shelter.Key)
	if err != nil {
		return nil, err
	}

	return animals.NewAnimals(g).Find(params.AnimalId, update.Key)
}

func UpdateAnimal(g *goon.Goon, animal *types.Animal, params *types.Params) error {
	oldAnimal, err := findAnimal(g, params)
	if err != nil {
		return err
	}

	shelter, err := shelters.NewShelters(g).Find(params.ShelterId)
	if err != nil {
		return err
	}

	update, err := updates.NewUpdates(g).Find(params.UpdateId, shelter.Key)
	if err != nil {
		return err
	}

	oldAnimal.Status = animal.Status
	oldAnimal.Name = animal.Name
	oldAnimal.SetFormattedValues()

	a := animals.NewAnimals(g)
	if err := a.Update(oldAnimal); err != nil {
		return err
	}

	newAnimal, err := a.Find(oldAnimal.Id, update.Key)
	if err != nil {
		return err
	}

	animal = newAnimal

	return nil
}

func PatchAnimal(g *goon.Goon, animal *types.Animal, params *types.Params) error {
	oldAnimal, err := findAnimal(g, params)
	if err != nil {
		return err
	}

	shelter, err := shelters.NewShelters(g).Find(params.ShelterId)
	if err != nil {
		return err
	}

	update, err := updates.NewUpdates(g).Find(params.UpdateId, shelter.Key)
	if err != nil {
		return err
	}

	for _, field := range params.Fields {
		switch field {
		case "status":
			oldAnimal.Status = animal.Status
			oldAnimal.SetFormattedValues()
		case "name":
			oldAnimal.Name = animal.Name
		}
	}

	a := animals.NewAnimals(g)
	if err := a.Update(oldAnimal); err != nil {
		return err
	}

	newAnimal, err := a.Find(oldAnimal.Id, update.Key)
	if err != nil {
		return err
	}

	animal = newAnimal

	return nil
}

func DeleteAnimal(g *goon.Goon, params *types.Params) error {
	animal, err := findAnimal(g, params)
	if err != nil {
		return err
	}

	animal.Status = types.AnimalStatusDeleted

	return animals.NewAnimals(g).Update(animal)
}

func findAnimal(g *goon.Goon, params *types.Params) (*types.Animal, error) {
	shelter, err := shelters.NewShelters(g).Find(params.ShelterId)
	if err != nil {
		return nil, err
	}

	update, err := updates.NewUpdates(g).Find(params.UpdateId, shelter.Key)
	if err != nil {
		return nil, err
	}

	return animals.NewAnimals(g).Find(params.AnimalId, update.Key)
}
