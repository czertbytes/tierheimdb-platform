package animals

import (
	"fmt"
	"net/http"

	"bitbucket.org/czertbytes/tierheimdb/pkg/auth"
	"bitbucket.org/czertbytes/tierheimdb/pkg/httputil"
	"bitbucket.org/czertbytes/tierheimdb/pkg/shelters"
	"bitbucket.org/czertbytes/tierheimdb/pkg/types"

	"github.com/julienschmidt/httprouter"
	"github.com/mjibson/goon"
)

// Creates a new Update entity.
// Requires authentication token in headers.
func Post(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	g := goon.NewGoon(r)

	sessionToken, err := httputil.GetSessionToken(r)
	if err != nil {
		g.Context.Debugf("Getting session token failed with error: %s", err)
		httputil.ResponseError(w, err)
		return
	}

	account, err := auth.NewAuth(g).FindAccount(sessionToken)
	if err != nil {
		g.Context.Debugf("Getting account failed with error: %s", err)
		httputil.ResponseError(w, err)
		return
	}

	shelterId, err := httputil.GetShelterId(ps)
	if err != nil {
		g.Context.Debugf("Getting shelterId failed with error: %s", err)
		httputil.ResponseError(w, err)
		return
	}

	shelter, err := shelters.NewShelters(g).Find(shelterId)
	if err != nil {
		g.Context.Debugf("Getting account failed with error: %s", err)
		httputil.ResponseError(w, err)
		return
	}

	if shelter.Key != account.Parent {
		if !account.IsAdmin() {
			g.Context.Debugf("Account is not authorized for this operation")
			httputil.ResponseUnauthorized(w)
			return
		}
	}

	updateId, err := httputil.GetUpdateId(ps)
	if err != nil {
		g.Context.Debugf("Getting shelterId failed with error: %s", err)
		httputil.ResponseError(w, err)
		return
	}

	params := &types.Params{
		SessionToken: sessionToken,
		ShelterId:    shelterId,
		UpdateId:     updateId,
	}

	var animal types.Animal
	if err := httputil.ParseBody(r, &animal); err != nil {
		g.Context.Debugf("Parsing animal body failed with error: %s", err)
		httputil.ResponseError(w, err)
		return
	}

	if err := CreateAnimal(g, &animal, params); err != nil {
		g.Context.Debugf("Creating animal failed with error: %s", err)
		httputil.ResponseError(w, err)
		return
	}

	url := fmt.Sprintf("http://api.tierheimdb.de/manager/v1/animals/%d", animal.Id)

	httputil.ResponseCreated(w, url, animal)
}

// Returns Animal entity.
func Get(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	g := goon.NewGoon(r)

	shelterId, err := httputil.GetShelterId(ps)
	if err != nil {
		g.Context.Debugf("Getting shelterId failed with error: %s", err)
		httputil.ResponseError(w, err)
		return
	}

	updateId, err := httputil.GetUpdateId(ps)
	if err != nil {
		g.Context.Debugf("Getting shelterId failed with error: %s", err)
		httputil.ResponseError(w, err)
		return
	}

	animalId, err := httputil.GetAnimalId(ps)
	if err != nil {
		g.Context.Debugf("Getting animalId failed with error: %s", err)
		httputil.ResponseError(w, err)
		return
	}

	params := &types.Params{
		ShelterId: shelterId,
		UpdateId:  updateId,
		AnimalId:  animalId,
	}

	animal, err := FindAnimal(g, params)
	if err != nil {
		g.Context.Debugf("Getting animal failed with error: %s", err)
		httputil.ResponseError(w, err)
		return
	}

	httputil.ResponseOK(w, animal)
}

// Updates Animal entity with values from body.
// Requires authentication token in headers.
func Put(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	g := goon.NewGoon(r)

	sessionToken, err := httputil.GetSessionToken(r)
	if err != nil {
		g.Context.Debugf("Getting session token failed with error: %s", err)
		httputil.ResponseError(w, err)
		return
	}

	account, err := auth.NewAuth(g).FindAccount(sessionToken)
	if err != nil {
		g.Context.Debugf("Getting account failed with error: %s", err)
		httputil.ResponseError(w, err)
		return
	}

	shelterId, err := httputil.GetShelterId(ps)
	if err != nil {
		g.Context.Debugf("Getting shelterId failed with error: %s", err)
		httputil.ResponseError(w, err)
		return
	}

	shelter, err := shelters.NewShelters(g).Find(shelterId)
	if err != nil {
		g.Context.Debugf("Getting account failed with error: %s", err)
		httputil.ResponseError(w, err)
		return
	}

	if shelter.Key != account.Parent {
		if !account.IsAdmin() {
			g.Context.Debugf("Account is not authorized for this operation")
			httputil.ResponseUnauthorized(w)
			return
		}
	}

	updateId, err := httputil.GetUpdateId(ps)
	if err != nil {
		g.Context.Debugf("Getting shelterId failed with error: %s", err)
		httputil.ResponseError(w, err)
		return
	}

	animalId, err := httputil.GetAnimalId(ps)
	if err != nil {
		g.Context.Debugf("Getting animalId failed with error: %s", err)
		httputil.ResponseError(w, err)
		return
	}

	params := &types.Params{
		SessionToken: sessionToken,
		ShelterId:    shelterId,
		UpdateId:     updateId,
		AnimalId:     animalId,
	}

	var animal types.Animal
	if err := httputil.ParseBody(r, &animal); err != nil {
		g.Context.Debugf("Parsing animal body failed with error: %s", err)
		httputil.ResponseError(w, err)
		return
	}

	if err := UpdateAnimal(g, &animal, params); err != nil {
		g.Context.Debugf("Updating animal failed with error: %s", err)
		httputil.ResponseError(w, err)
		return
	}

	httputil.ResponseOK(w, animal)
}

// Patches Animal entity with values from body. Patched fields are specified
// in request url in parameter fields. Field names are separated by comma.
// Requires authentication token in headers.
func Patch(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	g := goon.NewGoon(r)

	sessionToken, err := httputil.GetSessionToken(r)
	if err != nil {
		g.Context.Debugf("Getting session token failed with error: %s", err)
		httputil.ResponseError(w, err)
		return
	}

	account, err := auth.NewAuth(g).FindAccount(sessionToken)
	if err != nil {
		g.Context.Debugf("Getting account failed with error: %s", err)
		httputil.ResponseError(w, err)
		return
	}

	shelterId, err := httputil.GetShelterId(ps)
	if err != nil {
		g.Context.Debugf("Getting shelterId failed with error: %s", err)
		httputil.ResponseError(w, err)
		return
	}

	shelter, err := shelters.NewShelters(g).Find(shelterId)
	if err != nil {
		g.Context.Debugf("Getting account failed with error: %s", err)
		httputil.ResponseError(w, err)
		return
	}

	if shelter.Key != account.Parent {
		if !account.IsAdmin() {
			g.Context.Debugf("Account is not authorized for this operation")
			httputil.ResponseError(w, types.ErrUnauthorized)
			return
		}
	}

	updateId, err := httputil.GetUpdateId(ps)
	if err != nil {
		g.Context.Debugf("Getting shelterId failed with error: %s", err)
		httputil.ResponseError(w, err)
		return
	}

	animalId, err := httputil.GetAnimalId(ps)
	if err != nil {
		g.Context.Debugf("Getting animalId failed with error: %s", err)
		httputil.ResponseError(w, err)
		return
	}

	fields := httputil.GetFields(ps)

	params := &types.Params{
		SessionToken: sessionToken,
		ShelterId:    shelterId,
		UpdateId:     updateId,
		AnimalId:     animalId,
		Fields:       fields,
	}

	var animal types.Animal
	if err := httputil.ParseBody(r, &animal); err != nil {
		g.Context.Debugf("Parsing animal body failed with error: %s", err)
		httputil.ResponseError(w, err)
		return
	}

	if err := PatchAnimal(g, &animal, params); err != nil {
		g.Context.Debugf("Patching animal failed with error: %s", err)
		httputil.ResponseError(w, err)
		return
	}

	httputil.ResponseOK(w, animal)
}

// Deletes a Animal. It's not real deletion but setting status to deleted.
// Requires authentication token in headers.
func Delete(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	g := goon.NewGoon(r)

	sessionToken, err := httputil.GetSessionToken(r)
	if err != nil {
		g.Context.Debugf("Getting session token failed with error: %s", err)
		httputil.ResponseError(w, err)
		return
	}

	account, err := auth.NewAuth(g).FindAccount(sessionToken)
	if err != nil {
		g.Context.Debugf("Getting account failed with error: %s", err)
		httputil.ResponseError(w, err)
		return
	}

	shelterId, err := httputil.GetShelterId(ps)
	if err != nil {
		g.Context.Debugf("Getting shelterId failed with error: %s", err)
		httputil.ResponseError(w, err)
		return
	}

	shelter, err := shelters.NewShelters(g).Find(shelterId)
	if err != nil {
		g.Context.Debugf("Getting account failed with error: %s", err)
		httputil.ResponseError(w, err)
		return
	}

	if shelter.Key != account.Parent {
		if !account.IsAdmin() {
			g.Context.Debugf("Account is not authorized for this operation")
			httputil.ResponseError(w, types.ErrUnauthorized)
			return
		}
	}

	updateId, err := httputil.GetUpdateId(ps)
	if err != nil {
		g.Context.Debugf("Getting shelterId failed with error: %s", err)
		httputil.ResponseError(w, err)
		return
	}

	animalId, err := httputil.GetAnimalId(ps)
	if err != nil {
		g.Context.Debugf("Getting animalId failed with error: %s", err)
		httputil.ResponseError(w, err)
		return
	}

	params := &types.Params{
		SessionToken: sessionToken,
		ShelterId:    shelterId,
		UpdateId:     updateId,
		AnimalId:     animalId,
	}

	if err := DeleteAnimal(g, params); err != nil {
		g.Context.Debugf("Deleting animal failed with error: %s", err)
		httputil.ResponseError(w, err)
		return
	}

	httputil.ResponseNoContent(w)
}
