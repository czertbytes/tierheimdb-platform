package sync

import (
	"fmt"
	"net/http"

	"bitbucket.org/czertbytes/tierheimdb/pkg/httputil"
	"bitbucket.org/czertbytes/tierheimdb/pkg/types"

	"github.com/julienschmidt/httprouter"
	"github.com/mjibson/goon"
)

func Post(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	g := goon.NewGoon(r)

	sessionToken, err := httputil.GetSessionToken(r)
	if err != nil {
		g.Context.Debugf("Getting session token failed with error: %s", err)
		httputil.ResponseError(w, err)
		return
	}

	params := &types.Params{
		SessionToken: sessionToken,
	}

	var sync types.Sync
	if err := httputil.ParseBody(r, &sync); err != nil {
		httputil.ResponseError(w, err)
		g.Context.Debugf("Parsing sync body failed with error: %s", err)
		return
	}

	update, err := CreateSync(g, &sync, params)
	if err != nil {
		g.Context.Debugf("Creating sync failed with error: %s", err)
		httputil.ResponseError(w, err)
		return
	}

	url := fmt.Sprintf("http://api.tierheimdb.de/manager/v1/updates/%d", update.Id)

	httputil.ResponseCreated(w, url, sync)
}
