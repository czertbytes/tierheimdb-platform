package auth

import (
	"errors"
	"regexp"

	"bitbucket.org/czertbytes/tierheimdb/pkg/types"
)

const (
	EmailRegexp = `[a-z0-9!#$%&'*+/=?^_{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?`
)

var (
	ErrInvalidName     = errors.New("auth: Name is not valid.")
	ErrInvalidEmail    = errors.New("auth: Email is not valid.")
	ErrInvalidPassword = errors.New("auth: Password is not valid.")
)

func ValidateLogin(account *types.Account) error {
	if err := validateEmail(account.Email); err != nil {
		return err
	}

	if err := validatePassword(account.Password); err != nil {
		return err
	}

	return nil
}

func ValidateRegister(account *types.Account) error {
	if err := validateName(account.Name); err != nil {
		return err
	}

	if err := validateEmail(account.Email); err != nil {
		return err
	}

	if err := validatePassword(account.Password); err != nil {
		return err
	}

	return nil
}

func validateName(value string) error {
	if len(value) < 3 || len(value) > 255 {
		err := ErrInvalidName
		return types.BadRequest("Name length is not valid. Minimum length must be at least 3 characters.", err.Error(), err)
	}

	// TODO: complex validation

	return nil
}

func validateEmail(value string) error {
	if len(value) < 3 || len(value) > 255 {
		err := ErrInvalidEmail
		return types.BadRequest("Email length is not valid.", err.Error(), err)
	}

	if match, err := regexp.MatchString(EmailRegexp, value); err != nil || !match {
		return types.BadRequest("Email format is not valid.", err.Error(), err)
	}

	return nil
}

func validatePassword(value string) error {
	if len(value) < 8 || len(value) > 255 {
		err := ErrInvalidPassword
		return types.BadRequest("Password length is not valid. Minimum length must be at least 8 characters.", err.Error(), err)
	}

	// TODO: complex validation

	return nil
}
