package auth

import (
	"fmt"
	"net/http"

	"bitbucket.org/czertbytes/tierheimdb/pkg/httputil"
	"bitbucket.org/czertbytes/tierheimdb/pkg/types"

	"github.com/julienschmidt/httprouter"
	"github.com/mjibson/goon"
)

// Returns session information in account entity (token, session).
func Login(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	g := goon.NewGoon(r)

	var account types.Account
	if err := httputil.ParseBody(r, &account); err != nil {
		httputil.ResponseError(w, err)
		g.Context.Debugf("Parsing account body failed with error: %s", err)
		return
	}

	if err := CreateSession(g, &account); err != nil {
		httputil.ResponseError(w, err)
		g.Context.Debugf("Creating session failed with error: %s", err)
		return
	}

	httputil.ResponseOK(w, account)
}

// Invalidates session token.
// Requires authentication token in headers.
func Logout(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	g := goon.NewGoon(r)

	token, err := httputil.GetSessionToken(r)
	if err != nil {
		g.Context.Debugf("Getting session token failed with error: %s", err)
		httputil.ResponseError(w, err)
		return
	}

	if err := DeleteSession(g, token); err != nil {
		g.Context.Debugf("Deleting session failed with error: %s", err)
		httputil.ResponseError(w, err)
		return
	}

	httputil.ResponseNoContent(w)
}

// Creates an new account.
func Register(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	g := goon.NewGoon(r)

	var account types.Account
	if err := httputil.ParseBody(r, &account); err != nil {
		g.Context.Debugf("Parsing account body failed with error: %s", err)
		httputil.ResponseError(w, err)
		return
	}

	if err := CreateAccount(g, &account); err != nil {
		g.Context.Debugf("Creating account failed with error: %s", err)
		httputil.ResponseError(w, err)
		return
	}

	// Add location header path
	url := fmt.Sprintf("http://api.tierheimdb.com/manager/v1/accounts/%d", account.Id)

	httputil.ResponseCreated(w, url, account)
}
