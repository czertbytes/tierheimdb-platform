package auth

import (
	"bitbucket.org/czertbytes/tierheimdb/pkg/accounts"
	"bitbucket.org/czertbytes/tierheimdb/pkg/sessions"
	"bitbucket.org/czertbytes/tierheimdb/pkg/types"

	"github.com/mjibson/goon"
)

func CreateSession(g *goon.Goon, account *types.Account) error {
	if err := ValidateLogin(account); err != nil {
		return err
	}

	accounts, err := accounts.NewAccounts(g).FindAllByEmail(account.Email)
	if err != nil {
		return err
	}

	if len(accounts) == 0 {
		return types.BadRequest("Account not found!", types.ErrAccountNotFound.Error(), types.ErrAccountNotFound)
	}

	if !types.BCryptCompare(account.Password, accounts[0].Password) {
		return types.Unauthorized("Authorization failed!", types.ErrAccountNotFound.Error(), types.ErrAccountNotFound)
	}

	session := &types.Session{
		AccountId: accounts[0].Id,
	}
	if err := sessions.NewSessions(g).Create(session); err != nil {
		return err
	}

	account.Id = accounts[0].Id
	account.Status = accounts[0].Status
	account.CreatedAt = accounts[0].CreatedAt
	account.UpdatedAt = accounts[0].UpdatedAt
	account.Name = accounts[0].Name
	account.Password = ""
	account.Session = session.Token

	return nil
}

func DeleteSession(g *goon.Goon, token string) error {
	s := sessions.NewSessions(g)
	tokenSessions, err := s.FindAllByToken(token)
	if err != nil {
		return err
	}

	if len(tokenSessions) != 1 {
		return types.BadRequest("Account not found!", types.ErrAccountNotFound.Error(), types.ErrAccountNotFound)
	}

	if err := s.Delete(tokenSessions[0].Id); err != nil {
		return err
	}

	return nil
}

func CreateAccount(g *goon.Goon, account *types.Account) error {
	if err := ValidateRegister(account); err != nil {
		return err
	}

	if err := accounts.NewAccounts(g).Create(account); err != nil {
		return err
	}

	session := &types.Session{
		AccountId: account.Id,
	}
	if err := sessions.NewSessions(g).Create(session); err != nil {
		return err
	}

	account.Password = ""
	account.Session = session.Token

	return nil
}
