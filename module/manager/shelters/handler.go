package shelters

import (
	"fmt"
	"net/http"

	"bitbucket.org/czertbytes/tierheimdb/pkg/auth"
	"bitbucket.org/czertbytes/tierheimdb/pkg/httputil"
	"bitbucket.org/czertbytes/tierheimdb/pkg/types"

	"github.com/julienschmidt/httprouter"
	"github.com/mjibson/goon"
)

// Creates a new Shelter entity.
// Requires authentication token in headers.
func Post(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	g := goon.NewGoon(r)

	sessionToken, err := httputil.GetSessionToken(r)
	if err != nil {
		g.Context.Debugf("Getting session token failed with error: %s", err)
		httputil.ResponseError(w, err)
		return
	}

	params := &types.Params{
		SessionToken: sessionToken,
	}

	account, err := auth.NewAuth(g).FindAccount(sessionToken)
	if err != nil {
		g.Context.Debugf("Getting account failed with error: %s", err)
		httputil.ResponseError(w, err)
		return
	}

	// Only admin can create shelters
	if !account.IsAdmin() {
		g.Context.Debugf("Account is not authorized for this operation")
		httputil.ResponseUnauthorized(w)
		return
	}

	var shelter types.Shelter
	if err := httputil.ParseBody(r, &shelter); err != nil {
		g.Context.Debugf("Parsing shelter body failed with error: %s", err)
		httputil.ResponseError(w, err)
		return
	}

	if err := CreateShelter(g, &shelter, params); err != nil {
		g.Context.Debugf("Creating shelter failed with error: %s", err)
		httputil.ResponseError(w, err)
		return
	}

	url := fmt.Sprintf("http://api.tierheimdb.de/manager/v1/shelters/%d", shelter.Id)

	httputil.ResponseCreated(w, url, shelter)
}

// Returns all Shelter entities.
func GetAll(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	g := goon.NewGoon(r)

	shelters, err := FindShelters(g)
	if err != nil {
		g.Context.Debugf("Getting shelters failed with error: %s", err)
		httputil.ResponseError(w, err)
		return
	}

	response := map[string]interface{}{
		"shelters": shelters,
	}

	httputil.ResponseOK(w, response)
}

// Returns Shelter entity.
func Get(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	g := goon.NewGoon(r)

	shelterId, err := httputil.GetShelterId(ps)
	if err != nil {
		g.Context.Debugf("Getting shelterId failed with error: %s", err)
		httputil.ResponseError(w, err)
		return
	}

	params := &types.Params{
		ShelterId: shelterId,
	}

	shelter, err := FindShelter(g, params)
	if err != nil {
		g.Context.Debugf("Getting shelter failed with error: %s", err)
		httputil.ResponseError(w, err)
		return
	}

	httputil.ResponseOK(w, shelter)
}

// Updates Shelter entity with values from body.
// Requires authentication token in headers.
func Put(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	g := goon.NewGoon(r)

	sessionToken, err := httputil.GetSessionToken(r)
	if err != nil {
		g.Context.Debugf("Getting session token failed with error: %s", err)
		httputil.ResponseError(w, err)
		return
	}

	shelterId, err := httputil.GetShelterId(ps)
	if err != nil {
		g.Context.Debugf("Getting shelterId failed with error: %s", err)
		httputil.ResponseError(w, err)
		return
	}

	params := &types.Params{
		SessionToken: sessionToken,
		ShelterId:    shelterId,
	}

	account, err := auth.NewAuth(g).FindAccount(sessionToken)
	if err != nil {
		g.Context.Debugf("Getting account failed with error: %s", err)
		httputil.ResponseError(w, err)
		return
	}

	var shelter types.Shelter
	if err := httputil.ParseBody(r, &shelter); err != nil {
		g.Context.Debugf("Parsing shelter body failed with error: %s", err)
		httputil.ResponseError(w, err)
		return
	}

	if shelter.Key != account.Parent {
		if !account.IsAdmin() {
			g.Context.Debugf("Account is not authorized for this operation")
			httputil.ResponseUnauthorized(w)
			return
		}
	}

	if err := UpdateShelter(g, &shelter, params); err != nil {
		g.Context.Debugf("Updating shelter failed with error: %s", err)
		httputil.ResponseError(w, err)
		return
	}

	httputil.ResponseOK(w, shelter)
}

// Patches Shelter entity with values from body. Patched fields are specified
// in request url in parameter fields. Field names are separated by comma.
// Requires authentication token in headers.
func Patch(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	g := goon.NewGoon(r)

	sessionToken, err := httputil.GetSessionToken(r)
	if err != nil {
		g.Context.Debugf("Getting session token failed with error: %s", err)
		httputil.ResponseError(w, err)
		return
	}

	shelterId, err := httputil.GetShelterId(ps)
	if err != nil {
		g.Context.Debugf("Getting shelterId failed with error: %s", err)
		httputil.ResponseError(w, err)
		return
	}

	fields := httputil.GetFields(ps)

	params := &types.Params{
		SessionToken: sessionToken,
		ShelterId:    shelterId,
		Fields:       fields,
	}

	account, err := auth.NewAuth(g).FindAccount(sessionToken)
	if err != nil {
		g.Context.Debugf("Getting account failed with error: %s", err)
		httputil.ResponseError(w, err)
		return
	}

	var shelter types.Shelter
	if err := httputil.ParseBody(r, &shelter); err != nil {
		g.Context.Debugf("Parsing shelter body failed with error: %s", err)
		httputil.ResponseError(w, err)
		return
	}

	if shelter.Key != account.Parent {
		if !account.IsAdmin() {
			g.Context.Debugf("Account is not authorized for this operation")
			httputil.ResponseUnauthorized(w)
			return
		}
	}

	if err := PatchShelter(g, &shelter, params); err != nil {
		g.Context.Debugf("Patching shelter failed with error: %s", err)
		httputil.ResponseError(w, err)
		return
	}

	httputil.ResponseOK(w, shelter)
}

// Deletes a Shelter. It's not real deletion but setting status to deleted.
// Requires authentication token in headers.
func Delete(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	g := goon.NewGoon(r)

	sessionToken, err := httputil.GetSessionToken(r)
	if err != nil {
		g.Context.Debugf("Getting session token failed with error: %s", err)
		httputil.ResponseError(w, err)
		return
	}

	shelterId, err := httputil.GetShelterId(ps)
	if err != nil {
		g.Context.Debugf("Getting shelterId failed with error: %s", err)
		httputil.ResponseError(w, err)
		return
	}

	params := &types.Params{
		SessionToken: sessionToken,
		ShelterId:    shelterId,
	}

	account, err := auth.NewAuth(g).FindAccount(sessionToken)
	if err != nil {
		g.Context.Debugf("Getting account failed with error: %s", err)
		httputil.ResponseError(w, err)
		return
	}

	// Only admin can delete shelters
	if !account.IsAdmin() {
		g.Context.Debugf("Account is not authorized for this operation")
		httputil.ResponseUnauthorized(w)
		return
	}

	if err := DeleteShelter(g, params); err != nil {
		g.Context.Debugf("Deleting shelter failed with error: %s", err)
		httputil.ResponseError(w, err)
		return
	}

	httputil.ResponseNoContent(w)
}

// Returns a list of Animals in given Shelter.
func GetAnimals(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	g := goon.NewGoon(r)

	shelterId, err := httputil.GetShelterId(ps)
	if err != nil {
		g.Context.Debugf("Getting shelterId failed with error: %s", err)
		httputil.ResponseError(w, err)
		return
	}

	params := &types.Params{
		ShelterId: shelterId,
	}

	animals, err := FindAnimals(g, params)
	if err != nil {
		g.Context.Debugf("Getting shelter animals failed with error: %s", err)
		httputil.ResponseError(w, err)
		return
	}

	response := map[string]interface{}{
		"animals": animals,
	}

	httputil.ResponseOK(w, response)
}

// Returns a list of Updates in given Shelter.
func GetUpdates(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	g := goon.NewGoon(r)

	shelterId, err := httputil.GetShelterId(ps)
	if err != nil {
		g.Context.Debugf("Getting shelterId failed with error: %s", err)
		httputil.ResponseError(w, err)
		return
	}

	params := &types.Params{
		ShelterId: shelterId,
	}

	updates, err := FindUpdates(g, params)
	if err != nil {
		g.Context.Debugf("Getting shelter updates failed with error: %s", err)
		httputil.ResponseError(w, err)
		return
	}

	response := map[string]interface{}{
		"updates": updates,
	}

	httputil.ResponseOK(w, response)
}
