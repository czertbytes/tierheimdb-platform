package shelters

import (
	"bitbucket.org/czertbytes/tierheimdb/pkg/animals"
	"bitbucket.org/czertbytes/tierheimdb/pkg/shelters"
	"bitbucket.org/czertbytes/tierheimdb/pkg/types"
	"bitbucket.org/czertbytes/tierheimdb/pkg/updates"

	"github.com/mjibson/goon"
)

func CreateShelter(g *goon.Goon, shelter *types.Shelter, params *types.Params) error {
	return shelters.NewShelters(g).Create(shelter)
}

func FindShelters(g *goon.Goon) (types.Shelters, error) {
	return shelters.NewShelters(g).FindAll()
}

func FindShelter(g *goon.Goon, params *types.Params) (*types.Shelter, error) {
	return shelters.NewShelters(g).Find(params.ShelterId)
}

func UpdateShelter(g *goon.Goon, shelter *types.Shelter, params *types.Params) error {
	oldShelter, err := findShelter(g, params)
	if err != nil {
		return err
	}

	oldShelter.Status = shelter.Status
	oldShelter.Name = shelter.Name
	oldShelter.SetFormattedValues()

	s := shelters.NewShelters(g)
	if err := s.Update(oldShelter); err != nil {
		return err
	}

	newShelter, err := s.Find(oldShelter.Id)
	if err != nil {
		return err
	}

	shelter = newShelter

	return nil
}

func PatchShelter(g *goon.Goon, shelter *types.Shelter, params *types.Params) error {
	oldShelter, err := findShelter(g, params)
	if err != nil {
		return err
	}

	for _, field := range params.Fields {
		switch field {
		case "status":
			oldShelter.Status = shelter.Status
			oldShelter.SetFormattedValues()
		case "name":
			oldShelter.Name = shelter.Name
		}
	}

	s := shelters.NewShelters(g)
	if err := s.Update(oldShelter); err != nil {
		return err
	}

	newShelter, err := s.Find(oldShelter.Id)
	if err != nil {
		return err
	}

	shelter = newShelter

	return nil
}

func DeleteShelter(g *goon.Goon, params *types.Params) error {
	shelter, err := shelters.NewShelters(g).Find(params.ShelterId)
	if err != nil {
		return err
	}

	shelter.Status = types.ShelterStatusDeleted

	return shelters.NewShelters(g).Update(shelter)
}

func FindAnimals(g *goon.Goon, params *types.Params) (types.Animals, error) {
	shelter, err := shelters.NewShelters(g).Find(params.ShelterId)
	if err != nil {
		return nil, err
	}

	update, err := updates.NewUpdates(g).FindLatest(shelter.Key)
	if err != nil {
		return nil, err
	}

	return animals.NewAnimals(g).FindAllByParent(update.Key)
}

func FindUpdates(g *goon.Goon, params *types.Params) (types.Updates, error) {
	shelter, err := shelters.NewShelters(g).Find(params.ShelterId)
	if err != nil {
		return nil, err
	}

	return updates.NewUpdates(g).FindAllByParent(shelter.Key)
}

func findShelter(g *goon.Goon, params *types.Params) (*types.Shelter, error) {
	return shelters.NewShelters(g).Find(params.ShelterId)
}
