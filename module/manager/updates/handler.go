package updates

import (
	"fmt"
	"net/http"

	"bitbucket.org/czertbytes/tierheimdb/pkg/auth"
	"bitbucket.org/czertbytes/tierheimdb/pkg/httputil"
	"bitbucket.org/czertbytes/tierheimdb/pkg/shelters"
	"bitbucket.org/czertbytes/tierheimdb/pkg/types"

	"github.com/julienschmidt/httprouter"
	"github.com/mjibson/goon"
)

// Creates a new Update entity.
// Requires authentication token in headers.
func Post(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	g := goon.NewGoon(r)

	sessionToken, err := httputil.GetSessionToken(r)
	if err != nil {
		g.Context.Debugf("Getting session token failed with error: %s", err)
		httputil.ResponseError(w, err)
		return
	}

	account, err := auth.NewAuth(g).FindAccount(sessionToken)
	if err != nil {
		g.Context.Debugf("Getting account failed with error: %s", err)
		httputil.ResponseError(w, err)
		return
	}

	shelterId, err := httputil.GetShelterId(ps)
	if err != nil {
		g.Context.Debugf("Getting shelterId failed with error: %s", err)
		httputil.ResponseError(w, err)
		return
	}

	shelter, err := shelters.NewShelters(g).Find(shelterId)
	if err != nil {
		g.Context.Debugf("Getting account failed with error: %s", err)
		httputil.ResponseError(w, err)
		return
	}

	if shelter.Key != account.Parent {
		if !account.IsAdmin() {
			g.Context.Debugf("Account is not authorized for this operation")
			httputil.ResponseUnauthorized(w)
			return
		}
	}

	var update types.Update
	if err := httputil.ParseBody(r, &update); err != nil {
		g.Context.Debugf("Parsing update body failed with error: %s", err)
		httputil.ResponseError(w, err)
		return
	}

	params := &types.Params{
		SessionToken: sessionToken,
		ShelterId:    shelterId,
	}

	if err := CreateUpdate(g, &update, params); err != nil {
		g.Context.Debugf("Creating update failed with error: %s", err)
		httputil.ResponseError(w, err)
		return
	}

	url := fmt.Sprintf("http://api.tierheimdb.de/manager/v1/updates/%d", update.Id)

	httputil.ResponseCreated(w, url, update)
}

// Returns Update entity.
func Get(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	g := goon.NewGoon(r)

	shelterId, err := httputil.GetShelterId(ps)
	if err != nil {
		g.Context.Debugf("Getting shelterId failed with error: %s", err)
		httputil.ResponseError(w, err)
		return
	}

	updateId, err := httputil.GetUpdateId(ps)
	if err != nil {
		g.Context.Debugf("Getting updateId failed with error: %s", err)
		httputil.ResponseError(w, err)
		return
	}

	params := &types.Params{
		ShelterId: shelterId,
		UpdateId:  updateId,
	}

	update, err := FindUpdate(g, params)
	if err != nil {
		g.Context.Debugf("Getting update failed with error: %s", err)
		httputil.ResponseError(w, err)
		return
	}

	httputil.ResponseOK(w, update)
}

// Deletes an Update. It's not real deletion but setting status to deleted.
// Requires authentication token in headers.
func Delete(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	g := goon.NewGoon(r)

	sessionToken, err := httputil.GetSessionToken(r)
	if err != nil {
		g.Context.Debugf("Getting session token failed with error: %s", err)
		httputil.ResponseError(w, err)
		return
	}

	account, err := auth.NewAuth(g).FindAccount(sessionToken)
	if err != nil {
		g.Context.Debugf("Getting account failed with error: %s", err)
		httputil.ResponseError(w, err)
		return
	}

	shelterId, err := httputil.GetShelterId(ps)
	if err != nil {
		g.Context.Debugf("Getting shelterId failed with error: %s", err)
		httputil.ResponseError(w, err)
		return
	}

	shelter, err := shelters.NewShelters(g).Find(shelterId)
	if err != nil {
		g.Context.Debugf("Getting account failed with error: %s", err)
		httputil.ResponseError(w, err)
		return
	}

	if shelter.Key != account.Parent {
		if !account.IsAdmin() {
			g.Context.Debugf("Account is not authorized for this operation")
			httputil.ResponseUnauthorized(w)
			return
		}
	}

	updateId, err := httputil.GetUpdateId(ps)
	if err != nil {
		g.Context.Debugf("Getting updateId failed with error: %s", err)
		httputil.ResponseError(w, err)
		return
	}

	params := &types.Params{
		SessionToken: sessionToken,
		ShelterId:    shelterId,
		UpdateId:     updateId,
	}

	if err := DeleteUpdate(g, params); err != nil {
		g.Context.Debugf("Deleting update failed with error: %s", err)
		httputil.ResponseError(w, err)
		return
	}

	httputil.ResponseNoContent(w)
}

// Returns a list of Animal in given Update.
func GetAnimals(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	g := goon.NewGoon(r)

	shelterId, err := httputil.GetShelterId(ps)
	if err != nil {
		g.Context.Debugf("Getting shelterId failed with error: %s", err)
		httputil.ResponseError(w, err)
		return
	}

	updateId, err := httputil.GetUpdateId(ps)
	if err != nil {
		g.Context.Debugf("Getting updateId failed with error: %s", err)
		httputil.ResponseError(w, err)
		return
	}

	params := &types.Params{
		ShelterId: shelterId,
		UpdateId:  updateId,
	}

	animals, err := FindAnimals(g, params)
	if err != nil {
		g.Context.Debugf("Getting update animals failed with error: %s", err)
		httputil.ResponseError(w, err)
		return
	}

	response := map[string]interface{}{
		"animals": animals,
	}

	httputil.ResponseOK(w, response)
}
