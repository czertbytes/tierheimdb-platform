package updates

import (
	"bitbucket.org/czertbytes/tierheimdb/pkg/animals"
	"bitbucket.org/czertbytes/tierheimdb/pkg/shelters"
	"bitbucket.org/czertbytes/tierheimdb/pkg/types"
	"bitbucket.org/czertbytes/tierheimdb/pkg/updates"

	"github.com/mjibson/goon"
)

func CreateUpdate(g *goon.Goon, update *types.Update, params *types.Params) error {
	shelter, err := shelters.NewShelters(g).Find(params.ShelterId)
	if err != nil {
		return err
	}

	update.Parent = shelter.Key

	return updates.NewUpdates(g).Create(update)
}

func FindUpdate(g *goon.Goon, params *types.Params) (types.Update, error) {
	update, err := findUpdate(g, params)
	if err != nil {
		return types.Update{}, err
	}

	return *update, nil
}

func DeleteUpdate(g *goon.Goon, params *types.Params) error {
	update, err := findUpdate(g, params)
	if err != nil {
		return err
	}

	shelter, err := shelters.NewShelters(g).Find(params.ShelterId)
	if err != nil {
		return err
	}

	if update.Parent != shelter.Key {
		return types.Unauthorized("Unauthorized!", types.ErrUnauthorized.Error(), types.ErrUnauthorized)
	}

	update.Status = types.UpdateStatusDeleted

	return updates.NewUpdates(g).Update(update)
}

func FindAnimals(g *goon.Goon, params *types.Params) (types.Animals, error) {
	update, err := findUpdate(g, params)
	if err != nil {
		return nil, err
	}

	return animals.NewAnimals(g).FindAllByParent(update.Key)
}

func findUpdate(g *goon.Goon, params *types.Params) (*types.Update, error) {
	shelter, err := shelters.NewShelters(g).Find(params.ShelterId)
	if err != nil {
		return nil, err
	}

	return updates.NewUpdates(g).Find(params.UpdateId, shelter.Key)
}
