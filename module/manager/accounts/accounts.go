package accounts

import (
	"bitbucket.org/czertbytes/tierheimdb/pkg/accounts"
	"bitbucket.org/czertbytes/tierheimdb/pkg/shelters"
	"bitbucket.org/czertbytes/tierheimdb/pkg/types"

	"github.com/mjibson/goon"
)

func FindAccount(g *goon.Goon, params *types.Params) (*types.Account, error) {
	return accounts.NewAccounts(g).Find(params.AccountId)
}

func UpdateAccount(g *goon.Goon, account *types.Account, params *types.Params) error {
	oldAccount, err := findAccount(g, params)
	if err != nil {
		return err
	}

	oldAccount.Status = account.Status
	oldAccount.Name = account.Name
	oldAccount.SetFormattedValues()

	a := accounts.NewAccounts(g)
	if err := a.Update(oldAccount); err != nil {
		return err
	}

	newAccount, err := a.Find(oldAccount.Id)
	if err != nil {
		return err
	}

	account = newAccount

	return nil
}

func PatchAccount(g *goon.Goon, account *types.Account, params *types.Params) error {
	oldAccount, err := findAccount(g, params)
	if err != nil {
		return err
	}

	for _, field := range params.Fields {
		switch field {
		case "status":
			oldAccount.Status = account.Status
			oldAccount.SetFormattedValues()
		case "name":
			oldAccount.Name = account.Name
		}
	}

	a := accounts.NewAccounts(g)
	if err := a.Update(oldAccount); err != nil {
		return err
	}

	newAccount, err := a.Find(oldAccount.Id)
	if err != nil {
		return err
	}

	account = newAccount

	return nil
}

func DeleteAccount(g *goon.Goon, params *types.Params) error {
	account, err := accounts.NewAccounts(g).Find(params.AccountId)
	if err != nil {
		return err
	}

	account.Status = types.AccountStatusDeleted

	return accounts.NewAccounts(g).Update(account)
}

func FindShelter(g *goon.Goon, params *types.Params) (*types.Shelter, error) {
	account, err := findAccount(g, params)
	if err != nil {
		return nil, err
	}

	return shelters.NewShelters(g).Find(types.ShelterId(account.Parent.IntID()))
}

func findAccount(g *goon.Goon, params *types.Params) (*types.Account, error) {
	return accounts.NewAccounts(g).Find(params.AccountId)
}
