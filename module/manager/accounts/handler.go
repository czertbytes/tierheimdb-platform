package accounts

import (
	"net/http"

	"bitbucket.org/czertbytes/tierheimdb/pkg/auth"
	"bitbucket.org/czertbytes/tierheimdb/pkg/httputil"
	"bitbucket.org/czertbytes/tierheimdb/pkg/types"

	"github.com/julienschmidt/httprouter"
	"github.com/mjibson/goon"
)

// Returns Account entity.
func Get(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	g := goon.NewGoon(r)

	sessionToken, err := httputil.GetSessionToken(r)
	if err != nil {
		g.Context.Debugf("Getting session token failed with error: %s", err)
		httputil.ResponseError(w, err)
		return
	}

	accountId, err := httputil.GetAccountId(ps)
	if err != nil {
		g.Context.Debugf("Getting accountId failed with error: %s", err)
		httputil.ResponseError(w, err)
		return
	}

	params := &types.Params{
		SessionToken: sessionToken,
		AccountId:    accountId,
	}

	account, err := FindAccount(g, params)
	if err != nil {
		g.Context.Debugf("Getting account failed with error: %s", err)
		httputil.ResponseError(w, err)
		return
	}

	httputil.ResponseOK(w, account)
}

// Updates Account entity with values from body.
// Requires authentication token in headers.
func Put(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	g := goon.NewGoon(r)

	sessionToken, err := httputil.GetSessionToken(r)
	if err != nil {
		g.Context.Debugf("Getting session token failed with error: %s", err)
		httputil.ResponseError(w, err)
		return
	}

	accountId, err := httputil.GetAccountId(ps)
	if err != nil {
		g.Context.Debugf("Getting accountId failed with error: %s", err)
		httputil.ResponseError(w, err)
		return
	}

	account, err := auth.NewAuth(g).FindAccount(sessionToken)
	if err != nil {
		g.Context.Debugf("Getting account failed with error: %s", err)
		httputil.ResponseError(w, err)
		return
	}

	// Only admin can update accounts
	if !account.IsAdmin() {
		g.Context.Debugf("Account is not authorized for this operation")
		httputil.ResponseError(w, types.ErrUnauthorized)
		return
	}

	params := &types.Params{
		SessionToken: sessionToken,
		AccountId:    accountId,
	}

	if err := httputil.ParseBody(r, account); err != nil {
		g.Context.Debugf("Parsing account body failed with error: %s", err)
		httputil.ResponseError(w, err)
		return
	}

	if err := UpdateAccount(g, account, params); err != nil {
		g.Context.Debugf("Updating account failed with error: %s", err)
		httputil.ResponseError(w, err)
		return
	}

	httputil.ResponseOK(w, account)
}

// Patches Account entity with values from body. Patched fields are specified
// in request url in parameter fields. Field names are separated by comma.
// Requires authentication token in headers.
func Patch(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	g := goon.NewGoon(r)

	sessionToken, err := httputil.GetSessionToken(r)
	if err != nil {
		g.Context.Debugf("Getting session token failed with error: %s", err)
		httputil.ResponseError(w, err)
		return
	}

	accountId, err := httputil.GetAccountId(ps)
	if err != nil {
		g.Context.Debugf("Getting accountId failed with error: %s", err)
		httputil.ResponseError(w, err)
		return
	}

	account, err := auth.NewAuth(g).FindAccount(sessionToken)
	if err != nil {
		g.Context.Debugf("Getting account failed with error: %s", err)
		httputil.ResponseError(w, err)
		return
	}

	// Only admin can patch accounts
	if !account.IsAdmin() {
		g.Context.Debugf("Account is not authorized for this operation")
		httputil.ResponseError(w, types.ErrUnauthorized)
		return
	}

	fields := httputil.GetFields(ps)

	params := &types.Params{
		SessionToken: sessionToken,
		AccountId:    accountId,
		Fields:       fields,
	}

	if err := httputil.ParseBody(r, account); err != nil {
		g.Context.Debugf("Parsing account body failed with error: %s", err)
		httputil.ResponseError(w, err)
		return
	}

	if err := PatchAccount(g, account, params); err != nil {
		g.Context.Debugf("Patching account failed with error: %s", err)
		httputil.ResponseError(w, err)
		return
	}

	httputil.ResponseOK(w, account)
}

// Deletes a Account. It's not real deletion but setting status to deleted.
// Requires authentication token in headers.
func Delete(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	g := goon.NewGoon(r)

	sessionToken, err := httputil.GetSessionToken(r)
	if err != nil {
		g.Context.Debugf("Getting session token failed with error: %s", err)
		httputil.ResponseError(w, err)
		return
	}

	accountId, err := httputil.GetAccountId(ps)
	if err != nil {
		g.Context.Debugf("Getting accountId failed with error: %s", err)
		httputil.ResponseError(w, err)
		return
	}

	account, err := auth.NewAuth(g).FindAccount(sessionToken)
	if err != nil {
		g.Context.Debugf("Getting account failed with error: %s", err)
		httputil.ResponseError(w, err)
		return
	}

	// Only admin can delete accounts
	if !account.IsAdmin() {
		g.Context.Debugf("Account is not authorized for this operation")
		httputil.ResponseError(w, types.ErrUnauthorized)
		return
	}

	params := &types.Params{
		SessionToken: sessionToken,
		AccountId:    accountId,
	}

	if err := DeleteAccount(g, params); err != nil {
		g.Context.Debugf("Deleting account failed with error: %s", err)
		httputil.ResponseError(w, err)
		return
	}

	httputil.ResponseNoContent(w)
}

// Returns a Shelter for given Account.
func GetShelter(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	g := goon.NewGoon(r)

	sessionToken, err := httputil.GetSessionToken(r)
	if err != nil {
		g.Context.Debugf("Getting session token failed with error: %s", err)
		httputil.ResponseError(w, err)
		return
	}

	accountId, err := httputil.GetAccountId(ps)
	if err != nil {
		g.Context.Debugf("Getting accountId failed with error: %s", err)
		httputil.ResponseError(w, err)
		return
	}

	params := &types.Params{
		SessionToken: sessionToken,
		AccountId:    accountId,
	}

	shelter, err := FindShelter(g, params)
	if err != nil {
		g.Context.Debugf("Getting account shelter failed with error: %s", err)
		httputil.ResponseError(w, err)
		return
	}

	httputil.ResponseOK(w, shelter)
}
